# Constants
## APP_ID ##
| ID |    Name   |
|----|-----------|
|  1 |Bey2ollak  |
|  2 |WhyTraffic |

## DEVICE_TYPE ##
| ID |    Name   |
|----|-----------|
| 8  | I-Phone   |
| 10 | Android   |

## FORMAT ##
| ID | Name   |
|----|--------|
| 1  | XML    |
| 2  | JSON   |

## SCREEN/PAGE_ID ##
|   ID  | Name                               |
|-------|------------------------------------|
|---    | **Bey2ollak**                      |
| 1 	| BEY2OLLAK_ALL_ROADS                |
| 3 	| BEY2OLLAK_FAVORITES                |
| 2 	| BEY2OLLAK_COMMENTS                 |
| 4 	| BEY2OLLAK_ROUTE_ROADS 	     |
| 5     | BEY2OLLAK_NEARBY_ROADS_1           |
| 6 	| BEY2OLLAK_NEARBY_ROADS_2           |
| 7 	| BEY2OLLAK_MORNING_ROUTE_ROADS      |
| 8 	| BEY2OLLAK_EVENING_ROUTE_ROADS      |
| 9 	| BEY2OLLAK_BREAKING_ROUTE_ROADS     |
| 10 	| BEY2OLLAK_AVOID 	             |
| 11 	| BEY2OLLAK_AREAS 	             |
| 12 	| BEY2OLLAK_NEAR_ME 	             |
| 13 	| BEY2OLLAK_NOTIFICATION             |
| 14 	| BEY2OLLAK_PAGE 	             |
| 15 	| BEY2OLLAK_PROMOTIONS 	             |
|---    | **Why traffic**                    |
| 16 	| WHYTRAFFIC_ALL_ROADS 	             |
| 17 	| WHYTRAFFIC_COMMENTS 	             |
| 18 	| WHYTRAFFIC_FAVORITES 	             |
| 19 	| WHYTRAFFIC_NEARBY 	             |
| 20 	| WHYTRAFFIC_ROUTE_ROADS 	     |
| 21 	| WHYTRAFFIC_AVOID_FILTER 	     |
| 22 	| WHYTRAFFIC_AREAS_FILTER 	     |
| 23 	| WHYTRAFFIC_BOTTOM_AD 	             |
| 24 	| WHYTRAFFIC_PROMOTION 	             |

# Actions #
*mandatory parameters
## Common Parameter ##
|Name         | Tag       | Comment                  | 
|-------------|-----------|--------------------------| 
| *APP_ID     | appID     | Application Lookup       |
| *CITY_ID    | cityID    |                          |
| *USER_ID    | userID    |                          |
| DEVICE_TYPE | dev       | 8,10                     |
| VERSION     | ver       |                          |
| FORMAT      | format    | 1,2                      |
| DENSITY     | density   | 1,2,3,4,5                |
| LANGUAGE_ID | lang      | ar,en                    |

## ServeMessage ##
### Parameter ###
|Name         | Tag       | Comment                  | 
|-------------|-----------|--------------------------| 
| *SCREEN/PAGE_ID    | screenID    | Page Id Lookup           |
| RANK        | rank      |                          |
| ROADS       | roadIds     | CSV road ids             |
| AREAS       | areaIds     | CSV areas ids            |

### Sample ###
/MessageServer/MessageServlet?action=serveMessage&appID=1&cityID=1&userID=2&lang=ar&screenID=1

## getPromotion ##
### Parameter ###
N/A
### Sample ###
/MessageServer/MessageServlet?action=getPromotion&appID=2&cityID=2&userID=2&lang=ar

## getProfiles
### Parameter ###
N/A

### Sample ###
MessageServer/MessageServlet?action=getBrandProfiles&appID=1&cityID=1&userID=2&lang=ar

## getProfile ##
### Parameter ###
|Name         | Tag       | Comment                  | 
|-------------|-----------|--------------------------| 
| *BRAND_ID   | brandID   |                          |

### Sample ###
MessageServer/MessageServlet?action=getBrandProfile&brandID=1&appID=1&cityID=1&userID=2&lang=ar

## getBrandType
### Parameter ###
|Name         | Tag       | Comment                  | 
|-------------|-----------|--------------------------| 
| *BRAND_ID   | brandID   |                          |

### Sample ###
http://5.79.24.178/MessageServer/MessageServlet?action=serveMessage&appID=1&cityID=1&userID=2&lang=ar&screenID=1
#Response#
### Banner Ad ###

```
#!json

"ad": {
      "aid": "78",
      "type":1,
      "bgcl": "#F40000",
      "bgcls": "#231F20",
      "fncl": "#FFFFFF",
      "fncls": "#FFFFFF",
      "logo": "http://www.bey2ollak.com/downloads/logos/Ramadan-Truck-Hi-Res.png",
      "lid": "7",
      "cm": "3arabeyyet el far7a akid hatfag2ak!! law 3ayezha tegeelak doos hena we e3mel vote le mo7afzetak",
      "g": "1",
      "action" : {   
            "actionType" : "4",
            "actionValue" : ["1",”32231”],
      },
	      “lang”: “en”
}

```

### Web view(6) or Image(7) Ad ###

```
#!json

"ad": {
      "aid": "78",
      "type":6,
      "url": http://.....,
      "action" : {   
            "actionType" : "4",
            "actionValue" : ["1",”32231”],
      },
      “lang”: “en”
}
```